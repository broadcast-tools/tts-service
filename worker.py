import os
import logging
from pathlib import Path

import torch
from arq.connections import RedisSettings
from TTS.api import TTS

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

GIT_HASH = os.environ.get("GIT_HASH", "dev")
logging.info(f"Git Hash: {GIT_HASH}")

REDIS_HOST = os.environ.get("REDIS_HOST", "redis")
WAV_PATH = os.environ.get("WAV_PATH")

device = "cuda" if torch.cuda.is_available() else "cpu"


async def tts(
        ctx,
        text,
        model="xtts",
        language="de",
        speaker_wav="1.wav",
        emotion="Neutral",
        speed=1.0,
) -> str:
    job_id = ctx["job_id"]

    logging.info(f"Job ID: {job_id}")
    logging.info(f"Text: {text}")
    logging.info(f"Language: {language}")
    logging.info(f"Speaker Wav: {speaker_wav}")
    logging.info(f"Emotion: {emotion}")
    logging.info(f"Speed: {speed}")

    file_path = Path(WAV_PATH) / f"{job_id}.wav"
    speaker_wav_path = Path(WAV_PATH) / "speaker_wav" / speaker_wav

    tts = TTS(
        model_name=model,
        progress_bar=False
    ).to(device)

    tts.tts_to_file(
        text=text,
        language=language,
        speaker_wav=speaker_wav_path.as_posix(),
        file_path=file_path.as_posix(),
        emotion=emotion,
        speed=speed,
    )

    return file_path.as_posix()


async def startup(ctx):
    pass


class WorkerSettings:
    functions = [tts]
    redis_settings = RedisSettings(host=REDIS_HOST)
    on_startup = startup
    allow_abort_jobs = True
    job_timeout = 3600
    keep_result = 86400
