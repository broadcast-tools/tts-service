import os
import logging
from pathlib import Path
from enum import Enum
from typing_extensions import Annotated

from fastapi import FastAPI, WebSocket
from fastapi.responses import FileResponse, HTMLResponse
from fastapi.exceptions import HTTPException
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel, Field

from arq import create_pool
from arq.jobs import Job, JobStatus
from arq.connections import RedisSettings

logging.basicConfig(
    level=os.environ.get("LOGLEVEL", "INFO"),
    format='%(asctime)s %(message)s'
)

GIT_HASH = os.environ.get("GIT_HASH", "dev")
logging.info(f"Git Hash: {GIT_HASH}")

SENTRY_DSN = os.environ.get("SENTRY_DSN")
SENTRY_RELEASE = os.environ.get("SENTRY_RELEASE", GIT_HASH)
SENTRY_ENVIRONMENT = os.environ.get("SENTRY_ENVIRONMENT", "dev")
SENTRY_CA_CERTS = os.environ.get("SENTRY_CA_CERTS")

if SENTRY_DSN is not None:
    import sentry_sdk

    sentry_params = {
        "dsn": SENTRY_DSN,
        "release": SENTRY_RELEASE,
        "environment": SENTRY_ENVIRONMENT,
    }

    if SENTRY_CA_CERTS is not None:
        sentry_params["ca_certs"] = SENTRY_CA_CERTS

    sentry_sdk.init(**sentry_params)

REDIS_HOST = os.environ.get("REDIS_HOST", "redis")
WAV_PATH = os.environ.get("WAV_PATH", "./wav")


class Text(BaseModel):
    text: str


class TTSModel(str, Enum):
    xtts = "xtts"


class Emotion(str, Enum):
    NEUTRAL = "Neutral",
    HAPPY = "Happy",
    SAD = "Sad",
    SURPRISE = "Surprise",
    ANGRY = "Angry"


app = FastAPI(
    title="Text to Speech Webservice",
    description="Ein Text to Speech Webservice basiert auf Coqui.ai (https://github.com/coqui-ai/TTS)",
    version=GIT_HASH,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class JobResponse(BaseModel):
    """Base model for a job response.

    Attributes:
        job_id (str): The ID of the job.
        job_status (str): The status of the job.
    """

    job_id: str
    job_status: JobStatus


class JobCountResponse(BaseModel):
    """Base model for a job count response.

    Attributes:
        job_count (int): The number of jobs.
    """

    job_count: int


@app.get("/", include_in_schema=False)
async def get_root() -> HTMLResponse:
    return HTMLResponse(
        """
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Text to Speech</title>
    </head>
    <body>
        <input type="text" id="textInput" placeholder="Enter text here...">
        <button onclick="sendText()">Convert and Play</button>
        <br/>
        <audio controls id="audioPlayer">
            <source src="" type="audio/x-wav">
            Your browser does not support the audio element.
        </audio>

        <script>
            const ws = new WebSocket("ws://tts-service.swr.ard/ws");
            ws.onopen = (event) => {
                console.log("Connected to WebSocket");
            };
        
            ws.onmessage = (event) => {
                let jobId = event.data;
                document.getElementById("audioPlayer").src = `/tts/${jobId}/file`;
                document.getElementById("audioPlayer").load(); // Refreshes the audio player
                document.getElementById("audioPlayer").play(); // Plays the new audio
            };
        
            function sendText() {
                let text = document.getElementById("textInput").value;
                ws.send(text);
            }
        </script>
    </body>
</html>
        """
    )


@app.post("/tts/job", tags=["Text to Speech"])
async def create_text_to_speech_job(
        text: Text = Text(text="Hallo Welt!"),
        language: str = "de",
        speaker_wav: str = "1.wav",
        emotion: Emotion = Emotion.NEUTRAL,
        speed: float = 1.0
) -> JobResponse:
    logging.info(f"Text: {text}")

    job_queue = await create_pool(RedisSettings(host=REDIS_HOST))

    job = await job_queue.enqueue_job(
        function="tts",
        text=text.text,
        model=TTSModel.xtts.value,
        language=language,
        speaker_wav=speaker_wav,
        emotion=emotion.value,
        speed=speed,
    )
    job_status = await job.status()

    return JobResponse(job_id=job.job_id, job_status=job_status)


@app.get("/tts/job/count", tags=["Text to Speech"])
async def get_job_count() -> JobCountResponse:
    """Gets the count of jobs.

    Returns:
        JobCountResponse: A response containing the number of jobs.
    """
    job_queue = await create_pool(RedisSettings(host=REDIS_HOST, conn_timeout=60))
    queued_jobs = await job_queue.queued_jobs()

    return JobCountResponse(job_count=len(queued_jobs))


@app.get("/tts/job/{job_id}/status", tags=["Text to Speech"])
async def get_job_status(job_id: str) -> JobResponse:
    """Gets the status of a specific job.

    Args:
        job_id (str): The ID of the job.

    Returns:
        JobResponse: A response containing the job ID and status.
    """
    job_queue = await create_pool(RedisSettings(host=REDIS_HOST))
    job = Job(job_id=job_id, redis=job_queue)
    job_status = await job.status()

    return JobResponse(job_id=job_id, job_status=job_status)


@app.get("/tts/job/{job_id}/file", tags=["Text to Speech"])
async def get_file(job_id: str):
    file_path = Path(WAV_PATH) / f"{job_id}.wav"

    return FileResponse(f"{file_path}", media_type="audio/x-wav") if file_path.exists() else HTTPException(404)


@app.post("/tts/job/{job_id}/abort", tags=["Text to Speech"])
async def abort_job(job_id: str) -> JobResponse:
    """Aborts a specific job.

    Args:
        job_id (str): The ID of the job.

    Returns:
        JobResponse: A response containing the job ID and status.
    """
    job_queue = await create_pool(RedisSettings(host=REDIS_HOST, conn_timeout=60))
    job = Job(job_id=job_id, redis=job_queue)
    await job.abort()
    job_status = await job.status()

    return JobResponse(job_id=job_id, job_status=job_status)


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    text = await websocket.receive_text()

    job_queue = await create_pool(RedisSettings(host=REDIS_HOST))
    job = await job_queue.enqueue_job(
        function="tts",
        text=text,
        model=TTSModel.xtts.value,
    )
    await job.result(timeout=3600)

    await websocket.send_text(job.job_id)

if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="127.0.0.1", port=8000)
